import unittest

from src.coin_converter import str_to_int, int_to_str, num_odd_change_combinations, \
    all_change_combinations


class CoinConverterTestCases(unittest.TestCase):

    def test_bad_str(self):
        s = "BadString"
        self.assertRaises(ValueError, str_to_int, s)

        s = "£3.50"
        self.assertRaises(ValueError, str_to_int, s)

        s = "£3-50p"
        self.assertRaises(ValueError, str_to_int, s)

        s = "3-50"
        self.assertRaises(ValueError, str_to_int, s)

        s = "$3-50"
        self.assertRaises(ValueError, str_to_int, s)

    def test_str_to_int(self):
        s = "£3-50"
        actual = str_to_int(s)
        expected = 350
        self.assertAlmostEqual(expected, actual)

        s = "£03-550"
        actual = str_to_int(s)
        expected = 850
        self.assertAlmostEqual(expected, actual)

        s = "£2-"
        actual = str_to_int(s)
        expected = 200
        self.assertAlmostEqual(expected, actual)

    def test_float_to_str(self):
        f = 305
        actual = int_to_str(f)
        expected = "£3-5"
        self.assertEqual(expected, actual)

        f = 1150
        actual = int_to_str(f)
        expected = "£11-50"
        self.assertEqual(expected, actual)

    def test_num_odd_change_combinations(self):
        i = 5
        actual = num_odd_change_combinations(i)
        expected = 3
        self.assertEqual(expected, actual)

        i = 12
        actual = num_odd_change_combinations(i)
        expected = 7
        self.assertEqual(expected, actual)

    def test_all_change_combinations(self):
        i = 5
        actual = all_change_combinations(i)
        expected = [
            [5],
            [2, 2, 1],
            [2, 1, 1, 1],
            [1, 1, 1, 1, 1]
        ]
        self.assertEqual(len(expected), len(actual))
        for e, a in zip(expected, actual):
            self.assertTrue(e == a)

        i = 12
        actual = all_change_combinations(i)
        expected = [
            [10, 2],
            [10, 1, 1],
            [5, 5, 2],
            [5, 5, 1, 1],
            [5, 2, 2, 2, 1],
            [5, 2, 2, 1, 1, 1],
            [5, 2, 1, 1, 1, 1, 1],
            [5, 1, 1, 1, 1, 1, 1, 1],
            [2, 2, 2, 2, 2, 2],
            [2, 2, 2, 2, 2, 1, 1],
            [2, 2, 2, 2, 1, 1, 1, 1],
            [2, 2, 2, 1, 1, 1, 1, 1, 1],
            [2, 2, 1, 1, 1, 1, 1, 1, 1, 1],
            [2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
        ]
        self.assertEqual(len(expected), len(actual))
        for e, a in zip(expected, actual):
            self.assertTrue(e == a)
