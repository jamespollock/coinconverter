import re
from functools import lru_cache

COINS = (1, 2, 5, 10, 20, 50, 100, 200)


def str_to_int(ccy_str):
    if re.match(r"^£\d+-\d*$", ccy_str) is None:
        raise ValueError(f"Expected string in £xx-xx format, got '{ccy_str}' instead.")
    ccy_str = ccy_str.replace("£", "")
    pounds, pence = ccy_str.split("-")
    pence = 0 if pence == '' else pence
    return 100 * int(pounds) + int(pence)


def int_to_str(ccy_int):
    pounds = int(ccy_int / 100)
    pence = ccy_int - 100 * pounds
    return f"£{pounds}-{pence:.0f}"


@lru_cache(maxsize=256)
def all_change_combinations(ccy_int, allowed_coins=COINS):
    allowed_coins = sorted(allowed_coins, reverse=True)
    if ccy_int == 0:
        return [[]]
    possible_coins = [c for c in allowed_coins if c <= ccy_int]
    remainders = [ccy_int - c for c in possible_coins]
    combinations = []
    for coin, rem in zip(possible_coins, remainders):
        allowed_coins = tuple(c for c in possible_coins if c <= coin)
        combinations.extend([[coin] + comb for comb in all_change_combinations(rem, allowed_coins)])
    return combinations


def num_odd_change_combinations(ccy_int):
    even_combinations = [1] + [0] * ccy_int
    odd_combinations = [0] + [0] * ccy_int
    for coin in COINS:
        for i in range(coin, ccy_int + 1):
            even_combinations[i] += odd_combinations[i - coin]
            odd_combinations[i] += even_combinations[i - coin]
    return odd_combinations[ccy_int]


def y_n_answer(input_req):
    answered = False
    ans_num = 0
    while not answered and ans_num < 3:
        ans_num += 1
        req_str = input(input_req)
        if req_str.lower() not in ['y', 'n']:
            print(f"Did not understand answer: {req_str}, expected either 'y' or 'n'")
        else:
            answered = True
    return req_str.lower()


def coin_converter():
    str_err = True
    input_num = 0
    while str_err:
        input_num += 1
        try:
            ccy_str = input("Please enter a currency amount in the format £xx-xx: ")
            ccy_int = str_to_int(ccy_str)
            str_err = False
        except ValueError as e:
            print(str(e))
            if input_num == 3:
                print("Too many failed attempts")
                break
            print("Please re-enter the amount")

    if not str_err:
        res = num_odd_change_combinations(ccy_int)
        print(f"Number of odd change combinations: {res}")
        input_req = "WARNING Viewing combinations may cause memory issues if there are more than 100,000 to display."\
                    "\nWould you like to see the combinations y/n?"
        req_str = y_n_answer(input_req)
        if req_str == 'y':
            combs = [c for c in all_change_combinations(ccy_int) if len(c) % 2 == 1]
            for comb in combs:
                print([int_to_str(coin) for coin in comb])
