from src.coin_converter import coin_converter, y_n_answer


def main():
    end_program = False
    while not end_program:
        coin_converter()
        input_req = "Would you like to enter another amount y/n?"
        req_str = y_n_answer(input_req)
        end_program = False if req_str.lower() == 'y' else True


if __name__ == "__main__":
    main()
